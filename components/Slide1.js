import Head from 'next/head'
import Router from 'next/router'
import Link from 'next/link'


class Slide1 extends React.Component {
  render() {
    return (
  		<div className="slide-content">
    		<h1 className="slide1-text"  data-aos="fade-down" data-aos-duration="3000">
    			Explore
    			<br/>
    			new ways
    			<br/>
    			to garden
    		</h1>
      	<img src='/static/img/graphics3.png' className="slide1-img" data-aos="fade-up" data-aos-duration="3000"/>
      	<img src='/static/img/graphics1.png' className="slide1-img-g1" />
      	<img src='/static/img/graphics2.png' className="slide1-img-g2" />
      </div>
    )
  }
}

export default Slide1
