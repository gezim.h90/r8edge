import Head from 'next/head'
import Router from 'next/router'
import Link from 'next/link'


const links = [
  { text: 'ABOUT', link: '#' },
  { text: 'PRODUCTS & OFFERING', link: '#' },
  { text: 'SPACE & DESIGN', link: '#' },
  { text: 'CHALLENGE', link: '#' }
]

const langs = [
  { code: 'en', text: 'ENGLISH', selected: true },
  { code: 'ar', text: 'عَرَبِي' }
]

class Header extends React.Component {


  render() {
    return (
      <div>
        <nav className='nav-bar '>
          <ul>
            <li><a href="#" className="logo"><img src='/static/img/logo.png' /></a></li>
            {
              links.map((item) => (
                <li className='link' key={item.text}><a href={item.link}>{item.text}</a></li>
              ))
            }
            {/*TODO: Refactor */}
            <li className='separator flex'><img src='/static/img/line.png' /></li>
            <li className='selected'><a href='#'>ENGLISH</a></li>
            <li>/</li>
            <li><a href='#'>عَرَبِي</a></li>

          </ul>
        </nav>


        <style jsx>{`
          nav ul {
            list-style: none;
            padding: 0;
            margin: 0;
            align-items: center;
          }
          nav ul {
            display: flex;
            font-weight: bold;
          }
          nav ul li:first-child {
            margin-right: auto;
          }
          nav ul a {
            margin: 0em 1.8em;
            border-radius: 5px;
            color: black;
            display: block;
            text-decoration: none;
          }
          li.separator img {
            width: 70px;
          }
          nav ul li, nav ul a {
            position: relative;
          }
          /* Need to find a way in JSX to prevent this kind of repetition */
          nav ul li.selected a:before {
            content: ' ';
            background: #ff5c00;
            width: 100%;
            height: 5px;
            display: block;
            position: absolute;
            z-index: -1;
            height: 65%;
            top: 9px;
            left: 8px;
          }
          nav ul li:hover a:before {
            content: ' ';
            background: #ff5c00;
            width: 100%;
            height: 5px;
            display: block;
            position: absolute;
            z-index: -1;
            height: 65%;
            top: 9px;
            left: 8px;
          }
        `}</style>
      </div>
    )
  }
}

export default Header
