import Head from 'next/head'
import Router from 'next/router'
import Link from 'next/link'


class Slide3 extends React.Component {
  render() {
    return (
  		<div className="slide-content flex-row">
        <div className="flex-item mr-5">
          <img src='/static/img/in-house.png' alt='Area 2071' data-aos="fade-right" data-aos-duration="3000"/>
        </div>
        <div className="flex-item">
            <h1>
              Area 2071 <br/> Environment
            </h1>
            <p>
              In this space, innovators will meet and <br/>
              use the best technology of today to <br/>
              design the future
            </p>

            <a href='#' className='default'>LEARN MORE</a>
        </div>
        <img src='/static/img/vector-background.svg' className='slide3-background'/>


        <style jsx>{`
          .slide3 {
            margin: 0;
          }
          .slide3 .slide-content {
            height: 100%;
          }
          .slide3 .slide3-background {
            width: 50%;
            right: 0;
            position: absolute;
            z-index: -1;
          }
        `}</style>
      </div>
    )
  }
}

export default Slide3
