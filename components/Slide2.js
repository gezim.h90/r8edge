import Head from 'next/head'
import Router from 'next/router'
import Link from 'next/link'


class Slide2 extends React.Component {
  render() {
    return (
  		<div className="slide-content">
  			<div className="top">
      		<img src='/static/img/woman.png'  data-aos="fade-right" data-aos-duration="3000"/>
	    		<div className="ml-4">
	    			<h2 className='text-left'>What is Lorem Ipsum is Lorem Ips umis Lorem Ipsumis Lorem Ipsum ?</h2>
            <div className='flex-row main'>
              <p className='flex-item mr-2'>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised
                <br/>
                <br/>
                <u>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt uts.</u>
              </p>
              <p className='flex-item'>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised
              </p>
            </div>
	    		</div>
	    	</div>

	    	<div className="bottom">
	    		<div>
	    			<div>
	    				<div className='quote'>
                <h2 className='text-center'>We are building a new reality for our people, a new future <br/>
                for our children, and a new model of development.</h2>
                <span className='author'>Sheikh Mohammed bin Rashid</span>
              </div>
	    			</div>
	    		</div>
    		</div>
      </div>
    )
  }
}

export default Slide2
