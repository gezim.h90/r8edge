import Head from 'next/head'
import "../../src/styles.scss"


class Base extends React.Component {

  componentDidMount() {
  }

  render() {
    return (
      <div>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1.0"></meta>
          <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"></link>
          <link href="https://fonts.googleapis.com/css?family=Alfa+Slab+One" rel="stylesheet"></link>
          <link rel="stylesheet" href="/_next/static/style.css" />
          <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
          <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet"></link>
        </Head>

        <main className='content'>
          {this.props.children}
        </main>
      </div>
    )
  }
}

export default Base
