## R8edge project

### Run
First, install the dependencies:
```npm install```

Now, run:
```npm run dev```

### TO-DO

1. Move styles.scss under some subfolder. See plugin: https://github.com/zeit/next-plugins/tree/master/packages/next-sass
2. Find out the correct font types
3. Decided to not use a css framework (like Bootstrap). Strip down to basics for quicker page load.
4. For simplicity sake, graphics are created in Figma
5. Using a plugin for fullpage slides in order to save time
6. Missing responsiveness for the navbar
7. Dirty workaround: Header.js included in the first Slide of index.js. It should be included in the Base.js layout. This is for a quick solution to a margin problem.
8. Problem with the Slides when building the application
9. Check browser compatibility
10. Refactor CSS to be either in SASS or JSX. Having it in both places beats the purpose.
11. Refactor: Don't use CDN for the plugin
12. Use Next.js Link instead of html tag.
13. Folder name 'Layout' to love-case letter
14. Move helper classes which are in styles.scss into _helpers.scss

### Heroku
https://fast-tor-59084.herokuapp.com/

<br>


