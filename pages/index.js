import Base from '../components/Layout/Base.js';
import Slide1 from '../components/Slide1.js';
import Slide2 from '../components/Slide2.js';
import Slide3 from '../components/Slide3.js';
import Header from '../components/Header.js';
import { FullPage, Slide } from '../components/page-scroll/index.js';
import React from 'react'


export default class extends React.Component {
  componentDidMount() {
    AOS.init();
  }
  componentWillUpdate() {
  }
  render() {
    return <div>
      <Base>
        <FullPage >
          <Slide className="slide slide1">
            <Header/>
            <Slide1/>
          </Slide>
          <Slide className="slide slide2">
            <Slide2/>
          </Slide>
          <Slide className="slide slide3">
            <Slide3/>
          </Slide>
        </FullPage>
      </Base>

      <style jsx>{`
        .bg-graphics {
          position: relative;
        }
        .bg-graphics img {
          position: absolute;
        }
      `}</style>
    </div>
  }
}
